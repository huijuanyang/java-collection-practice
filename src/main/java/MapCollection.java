import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class MapCollection {
    
    public static List<Integer> doubleCollection(List<Integer> list) {
        // // Need to be implemented
        return list.stream().map(element -> element * 2).collect(Collectors.toList());
    }
    
    public static List<String> mapToStringCollection(List<Integer> list) {
        // Need to be implemented
        return list.stream().map(element -> String.valueOf((char)(element + 96))).collect(Collectors.toList());
    }
    
    public static List<String> uppercaseCollection(List<String> list) {
        // Need to be implemented
        return list.stream().map(element -> element.toUpperCase()).collect(Collectors.toList());
    }
    
    public static List<Integer> transformTwoDimensionalToOne(List<List<Integer>> list) {
        // Need to be implemented
        List<Integer> transformedList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            for (int j = 0; j < list.get(i).size(); j++) {
                transformedList.add(list.get(i).get(j));
            }
        }
        return transformedList;
    }
}

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class FilterCollection {
    
    public static List<Integer> getAllEvens(List<Integer> list) {
        // Need to be implemented
        return list.stream().filter(element -> element % 2 == 0).collect(Collectors.toList());
    }
    
    public static List<Integer> removeDuplicateElements(List<Integer> list) {
        // Need to be implemented
        return list.stream().distinct().collect(Collectors.toList());
    }
    
    public static List<Integer> getCommonElements(List<Integer> collection1, List<Integer> collection2) {
        // Need to be implemented
        Set<Integer> removeDuplicatedElementsOfCollection2 = new HashSet<>(collection2);
        List<Integer> getCommonElementsOfTwoCollections = collection1.stream()
                .filter(element -> !removeDuplicatedElementsOfCollection2.add(element))
                .distinct()
                .collect(Collectors.toList());;
        return getCommonElementsOfTwoCollections;
    }
}
